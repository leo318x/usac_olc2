from parser.entorno.Tipos import DataType
from parser.nodos.Nodo import Nodo
from parser.entorno.Entorno import Entorno


class InstruccionIf(Nodo):
    def __init__(self, token, id_nodo):
        super().__init__(token, id_nodo)

    def ejecutar(self, entorno):
        # if expresion { instrucciones }
        # if bool { instrucciones }
        self.hojas[1].ejecutar(entorno)
        if self.hojas[1].tipo == DataType.boolean:
            if self.hojas[1].valor:
                # instrucciones
                ne = Entorno("entornoif")
                ne.asignarAnterior(entorno)
                self.hojas[3].ejecutar(ne)
                print("Entorno de if", ne.tabla_simbolos)
            return
        print('El tipo de dato debe ser booleano ')
