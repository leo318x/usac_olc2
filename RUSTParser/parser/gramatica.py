from glob import glob
from parser.nodos import *

global noNode
noNode = 0

reserved = {
    'if': 'IF'
}

tokens = (
    'IF',
    'MAS',
    'MENOS',
    'POR',
    'DIVIDIDO',
    'DECIMAL',
    'ENTERO',
    'PTCOMA',
    'IGUAL',
    'IDENTIFICADOR',
    'CADENA',
    'LLAVEA',
    'LLAVEC',
    'MAYORQUE',
    'MENORQUE',
)

t_IF = r'if'
t_MAS = r'\+'
t_MENOS = r'-'
t_POR = r'\*'
t_DIVIDIDO = r'/'
t_PTCOMA = r';'
t_IGUAL = r'='
t_LLAVEA = r'{'
t_LLAVEC = r'}'
t_MAYORQUE = r'>'
t_MENORQUE = r'<'

def t_CADENA(t):
    r'\".*?\"'
    # Automata utilizado (ver grafico graphviz online)
    # digraph CADENA
    # {
    #     S1->S2[label = "''"];
    #     S2->S1[label = "''"];
    #     S2->S3[label = "{"];
    #     S3->S2[label = "{, }"];
    #     S2->S2[label = "Cualquier caracter"];
    #     S3->ERROR[label = "Cualquier caracter"];
    #     ERROR->ERROR[label = "Cualquier caracter"];
    # }
    t.value = t.value[1:-1]  # Quitamos las comillas
    nodoCadena = TerminalCadena(t, getNoNodo())
    estado = "S2"
    texto = ""
    for x in t.value:
        if x == "{":
            if estado == "S2":
                estado = "S3"
                continue
            elif estado == "S3":
                estado = "S2"
        elif x == "}":
            if estado == "S3":
                estado = "S2"
                nodoCadena.agregarTexto(texto, getNoNodo())
                nodoCadena.agregarFormato(getNoNodo())
                texto = ""
                continue
            else:
                estado = "ERROR"
        else:
            texto += x
    if texto != "":
        nodoCadena.agregarTexto(texto, getNoNodo())
    if estado == "S3":
        estado = "ERROR"
    if estado != "S2":
        print("Error con el formato de cadena", t.value)
        t.value = ""
        return t
    t.value = nodoCadena
    return t


def t_DECIMAL(t):
    r'\d+\.\d+'
    try:
        t.value = float(t.value)
    except:
        print("Error al parsear el flotante %d", t.value)
        t.value = 0
    return t


def t_ENTERO(t):
    r'\d+'
    try:
        t.value = int(t.value)
    except:
        print("Error al parsear el entero %d", t.value)
        t.value = 0
    return t


def t_IDENTIFICADOR(t):
    r'[a-zA-Z_][a-zA-Z_0-9]*'
    t.type = reserved.get(t.value, 'IDENTIFICADOR')  # Check for reserved words
    return t


t_ignore = " \t\n"


def t_error(t):
    print("Caracter invalido '%s'" % t.value[0])
    t.lexer.skip(1)


import ply.lex as lex

lexer = lex.lex()

precedence = (
    ('left', 'MAS', 'MENOS'),
    ('left', 'POR', 'DIVIDIDO')
)


def p_lista_instrucciones(t):
    '''instrucciones : instruccion instrucciones
                    | instruccion'''

    t[0] = InstruccionGenerico(t.slice[0], getNoNodo())
    if (len(t) == 3):
        t[0].hojas = t[2].hojas
        t[0].hojas.insert(0, t[1])
    else:
        t[0].hojas.append(t[1])


def p_instruccion_if(t):
    '''instruccion : instrif'''

    t[0] = t[1]


def p_instruccion_asignacion(t):
    '''instruccion : asignacion PTCOMA'''
    t[0] = t[1]


def p_funcion_if(t):
    '''instrif : IF expresion LLAVEA instrucciones LLAVEC'''
    t[0] = InstruccionIf(t.slice[0], getNoNodo())
    t[0].hojas.append(TerminalGenerico(t.slice[1], getNoNodo()))
    t[0].hojas.append(t[2])
    t[0].hojas.append(TerminalGenerico(t.slice[3], getNoNodo()))
    t[0].hojas.append(t[4])
    t[0].hojas.append(TerminalGenerico(t.slice[5], getNoNodo()))


def p_funcion_asignacion(t):
    '''asignacion : IDENTIFICADOR IGUAL expresion'''
    t[0] = InstruccionAsignacion(t.slice[0], getNoNodo())
    t[0].hojas.append(TerminalIdentificador(t.slice[1], getNoNodo()))
    t[0].hojas.append(TerminalGenerico(t.slice[2], getNoNodo()))
    t[0].hojas.append(t[3])


def p_expresion_suma(t):
    '''expresion : expresion MAS expresion'''
    t[0] = NodoSuma(t.slice[0], getNoNodo())
    t[0].hojas.append(t[1])
    t[0].hojas.append(TerminalGenerico(t.slice[2], getNoNodo()))
    t[0].hojas.append(t[3])


def p_expresion_resta(t):
    '''expresion : expresion MENOS expresion'''
    t[0] = NodoResta(t.slice[0], getNoNodo())
    t[0].hojas.append(t[1])
    t[0].hojas.append(TerminalGenerico(t.slice[2], getNoNodo()))
    t[0].hojas.append(t[3])


def p_expresion_multiplicacion(t):
    '''expresion : expresion POR expresion'''
    t[0] = NodoMultiplicacion(t.slice[0], getNoNodo())
    t[0].hojas.append(t[1])
    t[0].hojas.append(TerminalGenerico(t.slice[2], getNoNodo()))
    t[0].hojas.append(t[3])


def p_expresion_division(t):
    '''expresion : expresion DIVIDIDO expresion'''
    t[0] = NodoDivision(t.slice[0], getNoNodo())
    t[0].hojas.append(t[1])
    t[0].hojas.append(TerminalGenerico(t.slice[2], getNoNodo()))
    t[0].hojas.append(t[3])


def p_expresion_mayor(t):
    '''expresion : expresion MAYORQUE expresion'''
    t[0] = NodoMayor(t.slice[0], getNoNodo())
    t[0].hojas.append(t[1])
    t[0].hojas.append(TerminalGenerico(t.slice[2], getNoNodo()))
    t[0].hojas.append(t[3])


def p_expresion_cadena(t):
    '''expresion : CADENA'''
    t[0] = t.slice[1].value


def p_expresion_entero(t):
    '''expresion : ENTERO'''
    t[0] = TerminalEntero(t.slice[1], getNoNodo())


def p_expresion_decimal(t):
    '''expresion : DECIMAL'''
    t[0] = TerminalDecimal(t.slice[1], getNoNodo())


def p_expresion_decimal(t):
    '''expresion : IDENTIFICADOR'''
    t[0] = TerminalIdentificador(t.slice[1], getNoNodo())

def p_error(t):
    print("Error sintáctico %s" % t.value)


import ply.yacc as yacc

parser = yacc.yacc()


def getNoNodo():
    global noNode
    noNode = noNode + 1
    return noNode


def analizar_entrada(entrada_texto):
    return parser.parse(entrada_texto)
