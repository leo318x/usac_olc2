fn main(){
	let mut xyz: i64 = 10;
	xyz = 5;
	xyz = 20+3;
	println!("{}", xyz+2);
}
/*#####RESULTADO#####
25
*/


fn main(){
	let mut varx = 8;
	let mut vary = 6;
	
	if varx > vary{
		let mut suma = varx + vary;
		println!("{}",suma);
		if suma == 15{
			let mut resta = suma-varx;
			println!("{}",resta);
		}else{
			let multi:i64 = 20;
			println!("{}",multi*suma);
		}
	}
}
/*####RESULTADO####
14
280
*/


fn main(){
	if 3>2 {
		println!("verdadero");
	} else if 5<2 {
		println!("elif1");
	} else if true != false {
		println!("elif2");
	} else if 5 == 5 {
		println!("elif 3");
	}else if true == true {
		println!("elif 4");
	}
}
/*####RESULTADO####
verdadero
*/

fn main(){
	let mut varx = 10;
	let vary = 1;
	let varz = 2;
	
	
	if varz < vary{
	    println!("Primer if");   
	}else if varx > vary{
	    println!("Primer else if");
	    if true{
	    let mut varm = 31;
	    let mut suma = vary+varz;
	        if true{
	            if true{
	                println!("If anidado 3er nivel");
	                println!("{}",suma+varm); //34
									let mut var1 = true;
									varx = 0;
									while var1 {
									println!("{}",varx);
									if varx == 20{
									var1 = false;
									}
									varx = varx + 1;
									}
	            }
	        }
	    }
	    println!("Prueba finalizada correctamente"); 
	}
}

/*#####RESULTADO#####
Primer else if
If anidado 3er nivel
34
0
1
2
3
4
5
6
7
8
9
10
11
12
13
14
15
16
17
18
19
20
Prueba finalizada correctamente
*/

fn main() {
    let mut var1 = true;
    let mut varx = 0;
    while var1 {
      println!("{}",varx);
      if varx == 20{
        var1 = false;
      }
      println!("Antes del break");
      break;
      println!("Despues del break");
      varx = varx + 1;
    }
  }

  
/*####### RESULTADO
0
Antes del break
*/

fn main() {
    let mut var1 = 0;
    let mut varx = 0;
    while var1<10 {
      println!("{}",var1);
      var1 = var1 + 1;
      while varx < 2{
          if var1 >= 5{
          break;
          }
        println!("x");
        varx = varx+1;
      }
      varx = 0;
    }
  }

/*#####RESULTADO#####
0
x
x
1
x
x
2
x
x
3
x
x
4
5
6
7
8
9
*/

fn main(){
    let mut var1 = 0;
      let mut varx = 0;
      let mut vary = true;
      while var1 <= 3 {
        println!("Primer While");
        while varx <= 2{
            println!("Segundo While");
            varx = varx + 1;
            while vary{
                println!("Tercer While");
                break;
                            println!("No lo imprime");
            }
        }
        var1 = var1+1;
      }
    }

/*#####RESULTADO#####
Primer While
Segundo While
Tercer While
Segundo While
Tercer While
Segundo While
Tercer While
Primer While
Primer While
Primer While
*/

fn main(){
	let varx = 10;
	let mut vary = 6;
		while vary <= varx {
		    println!("{}",vary);
			if vary == 8{
				println!("Rompo el while");
			break;
			}
		vary = vary + 1;
		}
}

/*#####RESULTADO#####
6
7
8
Rompo el while
*/

fn main(){
    let varx = 20;
    let mut vary = 0;
    while vary <= varx {
        println!("{}",vary);
        if vary == 6{
        if true{
            if true{
                if true{
                    if false{
                       println!("Aquí no");
                    }else if false{
                        println!("Aquí tampoco");
                    }else if true{
                       println!("Aquí rompemos :)");
                       break; 
                    }else{
                       println!("Aquí no rompemos");
                                     break; 
                    }
                }
            }
        }
    }
    vary = vary + 1;
    }
}

/*####RESULTADO####
0
1
2
3
4
5
6
Aquí rompemos :)
*/

fn main(){
    let varx = 3;
    let mut vary = 0;
    let mut varz = 1;
    while vary <= varx {
        println!("{}",vary);
        if true{
            if true{
                if true{
                    while true{
                        println!("^:)");
                        if varz == 3{
                            break;
                        }
                        varz = varz + 1;
                    }
                }
            }
        }
        varz = 1;
        vary = vary + 1;
    }
}

/*####RESULTADO####
0
^:)
^:)
^:)
1
^:)
^:)
^:)
2
^:)
^:)
^:)
3
^:)
^:)
^:)
*/