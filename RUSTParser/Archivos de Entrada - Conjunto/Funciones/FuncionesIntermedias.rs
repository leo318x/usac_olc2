fn main() {
    let mut varmutable: i64 = 78;

    println!("IF");

    if varmutable > 50 {
        println!("Si sale :3");
    }else if true && (varmutable == 56) {
        println!("Me tengo que esforzar, ya casi :3");
    } else {
        println!("Aún no, pero se puede");
    }

    varmutable = 90;
    if varmutable > 0 {
        println!("Vamos por buen camino");
        if varmutable == 7 {
            println!("Aun no");
        } else if varmutable > 10 && varmutable < 100 {
            println!("Casi casi");
            if varmutable == 90 {
                println!("Excelente ");
                varmutable = 20000;
            }

        } else {
            println!("Sigo intentando");
        }
    }else if varmutable <= 3 {
        println!("Upss, nos pasamos");
        if true && (varmutable == 1) {
            println!("Nop, revisemos los ifs");
        } else if varmutable > 10 {
            println!("Nop, revisemos los ifs");
        } else {
            println!("Solo debemos revisar algunos ifs");
        }
    } else if varmutable != varmutable {
        println!("Upss, nos pasamos");
        if true && (varmutable == -1) {
            println!("Nop, revisemos los ifs");
        } else if varmutable > 10 {
            println!("Nop, revisemos los ifs");
        } else {
            println!("Solo debemos revisar algunos ifs");
        }
    }

    println!("El valor e la variable mutable es : {} y debería ser 20000", varmutable);
    if (varmutable == 20000){
        println!("Excelente, los ifs están correctos");
    }


}