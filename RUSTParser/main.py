# --------------------------------------
# Universidad de San Carlos de Guatemala
# Ingenieria en Ciencias y Sistemas
# Eduardo Isai Ajsivinac Xico
# 201503584
# RUST Parser
from parser.entorno.Entorno import Entorno
from parser.gramatica import analizar_entrada

def print_hi(text_input):
    raiz = analizar_entrada(text_input)
    print(raiz.obtener_dot())
    entorno = Entorno("Global")
    raiz.ejecutar(entorno)
    print("Entorno Global", entorno.tabla_simbolos)


# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    archivo = open("./entradas/entrada.rs")
    linea = archivo.readline()
    texto_entrada = linea
    while linea != '':
        linea = archivo.readline()
        texto_entrada += linea
    print_hi(texto_entrada)

# See PyCharm help at https://www.jetbrains.com/help/pycharm/
